**Orange Theory Fitness Coding Assignment, implemented by Jason Braswell**

---

## Notes

1. Decided to forgo the unit testing on the DAL due to the time effort & difficulty in getting all the proper mocking libraries to work (first time working with those particular ones). I also figured there was enough content in this project for the team to get a general sense.

2. Made an assumption on how I was supposed to incorporate the Random User Generator API - applied it to the Create operation in lieu of the typical request body-supplied payload. Wasn't able to get any feedback or specificity on requirements.
const { expect } = require('chai');
const responseUtils = require('../../utils/response-utils');

describe('Response Utilities', () => {
    it('formats the response body of an error', () => {
        const msg = "This is a test";
        const expected = { message: msg };
        const actual = responseUtils.formatErrorBody(msg);
        expect(actual).to.deep.equals(expected);
    });
});
const { expect } = require('chai');
const nodeMocks = require('node-mocks-http');
const pagination = require('../../middleware/pagination-param-parser');

describe('Pagination param parsing middleware', () => {
    const nextRoute = () => nextRouteCalled = true;
    let request, response, nextRouteCalled;

    beforeEach(() => {
        request = nodeMocks.createRequest();
        response = nodeMocks.createResponse();
    });

    afterEach(() => {
        nextRouteCalled = false;
    });

    it('updates the request obj with the default values if not specified', () => {
        // Setup
        const expected = { maxResults: 17 };

        // Execute
        const middleware = pagination(17);
        middleware(request, response, nextRoute);

        // Test
        expect(nextRouteCalled).to.be.true;
        expect(request.pagination).to.deep.equal(expected);
    });

    it('updates the request obj when max results is a query param', () => {
        // Setup
        const expected = { maxResults: 32 };
        request.query.maxResults = "32";

        // Execute
        const middleware = pagination(100);
        middleware(request, response, nextRoute);

        // Test
        expect(nextRouteCalled).to.be.true;
        expect(request.pagination).to.deep.equal(expected);
    });

    it('fails when max results is NaN', () => {
        // Setup
        request.query.maxResults = "abc";

        // Execute
        const middleware = pagination(100);
        middleware(request, response, nextRoute);

        // Test
        expect(nextRouteCalled).to.be.false;
        expect(response.statusCode).to.equal(400);
    });

    it('fails when max results is less than 1 ', () => {
        // Setup
        request.query.maxResults = 0;

        // Execute
        const middleware = pagination(100);
        middleware(request, response, nextRoute);

        // Test
        expect(nextRouteCalled).to.be.false;
        expect(response.statusCode).to.equal(400);
    });
});
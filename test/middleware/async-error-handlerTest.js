const { expect } = require('chai');
const nodeMocks = require('node-mocks-http');
const asyncErrorHandler = require('../../middleware/async-error-handler');

describe('Async error handler', () => {
    let request, response;

    beforeEach(() => {
        request = nodeMocks.createRequest();
        response = nodeMocks.createResponse();
    });

    it('reponds with a 500 error when unknown errors occur', async () => {
        // Setup
        const route = async (req, res, next) => { throw new Error("Logged testing error"); };
        const wrappedRoute = asyncErrorHandler(route);

        // Execute
        await wrappedRoute(request, response);

        // Test
        expect(response.statusCode).to.equal(500);
        expect(JSON.parse(response._getData())).to.deep.equal({ message: "Internal Server Error"});
    });

    it('responds with an HTTP code and custom message with known errors', async () => {
        // Setup
        const expectedHttpCode = 429;
        const expectedErrorMsg = "Failed depedency error";
        const route = async (req, res, next) => {
            const customError = new Error(expectedErrorMsg);
            customError.httpCode = expectedHttpCode;
            throw customError;
        };
        const wrappedRoute = asyncErrorHandler(route);

        // Execute
        await wrappedRoute(request, response);

        // Test
        expect(response.statusCode).to.equal(expectedHttpCode);
        expect(JSON.parse(response._getData())).to.deep.equal({ message: expectedErrorMsg});
    });
});
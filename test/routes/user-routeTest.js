const sinon = require('sinon');
const sandbox = require('sinon-test')(sinon);
const { expect } = require('chai');
const app = require('express')();
const bodyParser = require('body-parser');
const supertest = require('supertest');
const { UserComponent } = require('otf-components');

// Enable JSON parsing for our custom NodeJS app
app.use(bodyParser.json({ strict: false }));

// Load the route to test
require('../../routes/user-route')(app);

// Enable HTTP testing
const request = supertest(app);

describe('GET /users/:id (Get User)', () => {
    it('should respond with 200 OK and a User object', sandbox(async function () {
        // Setup
        const user = { name: "Jason" };
        const userComponentMock = this.mock(UserComponent);
        userComponentMock.expects('getById').withArgs("42").resolves(user);

        // Execute
        const response = await request.get('/users/42');

        // Test
        expect(response.statusCode).to.equal(200);
        expect(response.body).to.deep.equal(user);
    }));

    it('should respond with 500 when an error occurs', sandbox(async function () {
        // Setup
        const userComponentMock = this.mock(UserComponent);
        userComponentMock.expects('getById').throws(new Error());

        // Execute
        const response = await request.get('/users/10');

        // Test
        expect(response.statusCode).to.equal(500);
    }));
});

describe('GET /users (List Users)', () => {
    it('should respond with 200 and a list of User objects', sandbox(async function () {
        // Setup
        const users = [{ name: "Arya" }, { name: "Sansa" }];
        const userComponentMock = this.mock(UserComponent);
        userComponentMock.expects('list').withArgs({ maxResults: 100 }).resolves(users);

        // Execute
        const response = await request.get('/users');

        // Test
        expect(response.statusCode).to.equal(200);
        expect(response.body).to.deep.equal(users);
    }));

    it('should accept pagination parameters', sandbox(async function () {
        // Setup
        const users = [{ name: "Cold Hands" }];
        const userComponentMock = this.mock(UserComponent);
        userComponentMock.expects('list').withArgs({ maxResults: 21 }).resolves(users);


        // Execute
        const response = await request.get('/users?maxResults=21');

        // Test
        expect(response.statusCode).to.equal(200);
        expect(response.body).to.deep.equal(users);
    }));

    it('should respond with 500 when an error occurs', sandbox(async function () {
        // Setup
        const userComponentMock = this.mock(UserComponent);
        userComponentMock.expects('list').throws(new Error());

        // Execute
        const response = await request.get('/users');

        // Test
        expect(response.statusCode).to.equal(500);
    }));
});

describe('PUT /users/:id (Update User)', () => {
    it('should respond with 200', sandbox(async function () {
        // Setup
        const userComponentMock = this.mock(UserComponent);
        userComponentMock.expects('update').withArgs("90", {}).resolves();

        // Execute
        const response = await request.put('/users/90');

        // Test
        expect(response.statusCode).to.equal(200);
    }));

    it('should respond with 500 when an error occurs', sandbox(async function () {
        // Setup
        const userComponentMock = this.mock(UserComponent);
        userComponentMock.expects('update').throws(new Error());

        // Execute
        const response = await request.put('/users/90');

        // Test
        expect(response.statusCode).to.equal(500);
    }));
});

describe('POST /users (Create User)', () => {
    it('should respond with 201 and a User object', sandbox(async function () {
        // Setup
        const user = { name: "Ned" };
        const userComponentMock = this.mock(UserComponent);
        userComponentMock.expects('createRandomUser').resolves(user);

        // Execute
        const response = await request.post('/users');

        // Test
        expect(response.statusCode).to.equal(201);
    }));

    it('should respond with 500 when an error occurs', sandbox(async function () {
        // Setup
        const userComponentMock = this.mock(UserComponent);
        userComponentMock.expects('createRandomUser').throws(new Error());

        // Execute
        const response = await request.post('/users');

        // Test
        expect(response.statusCode).to.equal(500);
    }));
});
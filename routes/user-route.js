const express = require('express');
const asyncErrorHandler = require('../middleware/async-error-handler');
const paginated = require('../middleware/pagination-param-parser');
const { UserComponent } = require('otf-components');

const getUser = asyncErrorHandler(async (req, res) => {
    const user = await UserComponent.getById(req.params.id);
    res.json(user);
});

const listUsers = asyncErrorHandler(async (req, res) => {
    const users = await UserComponent.list(req.pagination);
    res.json(users);
});

const updateUser = asyncErrorHandler(async (req, res) => {
    await UserComponent.update(req.params.id, req.body);
    res.send();
});

const createUser = asyncErrorHandler(async (req, res) => {
    const createdUser = await UserComponent.createRandomUser();
    res.status(201).json(createdUser);
});

module.exports = (app) => {
    const router = express.Router();
    app.use('/users', router);

    router.get('/:id', getUser);
    router.get('/', [paginated(100)], listUsers);
    router.put('/:id', updateUser);
    router.post('/', createUser);
};
module.exports = {
    /**
     * Formats the body of an HTTP response containing an error
     */
    formatErrorBody: (message) => {
        return { message: message };
    }
};
const { expect } = require('chai');
const randomUserApi = require('../random-user-generator');

describe('Random User Generator API', () => {
    it('Generates a random user', async () => {
        const user = await randomUserApi.generate();
        
        expect(user).to.not.be.null;
        expect(user.login).to.be.undefined;
    }).timeout(10000);
});


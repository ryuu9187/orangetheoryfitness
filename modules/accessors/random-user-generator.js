const fetch = require('node-fetch');
const RANDOM_USER_GENERATOR_API = "https://randomuser.me/api/";

module.exports = {
    /**
     * Generates a random user
     * @returns a random User object
     */
    generate: async () => {
        const response = await fetch(RANDOM_USER_GENERATOR_API);
        const generatedUsers = await response.json();
        const user = generatedUsers.results[0];

        delete user.login; // Remove login info
        return user;
    }
};
const UUID = require('node-uuid');
const USERS_TABLE = process.env.USERS_TABLE;
const Repository = require("./repository")(USERS_TABLE);
const UserRepository = Repository;

/**
 * Creates a new User
 * @param {Object} user the User model to persist
 */
UserRepository.create = async (user) => {
    user.id = UUID.v4();
    await Repository.save(user);
    return user;
};

/**
 * Retrieves a User by its unique identifier
 * @param {String} id the id of the user
 * @returns the matching User
 */
UserRepository.getById = async (id) => {
    return await Repository.getByKey({ id: id });
};

/**
 * Updates all of the attributes of a User
 * @param {Object} user the User to update
 */
UserRepository.update = async (user) => {
    const saveCondition = {
        expr: 'id = :id',
        values: { ':id': user.id }
    };
    
    await Repository.save(user, saveCondition);
};

module.exports = UserRepository;
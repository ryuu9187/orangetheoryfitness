const sinon = require('sinon');
const sandbox = require('sinon-test')(sinon);
const { expect } = require('chai');
const { errors } = require('otf-common');

const FAKE_TABLE_NAME = "Users";
const repositoryFactory = require('../../repositories/repository');
const repository = repositoryFactory(FAKE_TABLE_NAME);
const awsMock = require('aws-sdk-mock');

describe('Repository GetByKey', () => {

    afterEach(() => {
        // awsMock.restore('DynamoDB.DocumentClient');
        // awsMock.restore('DynamoDB');
    });

    it('returns the DDB item when found', async () => {
        /*// Setup
        const item = { name: 'Jorah' };
        const key = { Id: '123-abc' };
        awsMock.mock('DynamoDB.DocumentClient', 'get', (params, cb) => {
            expect(params.TableName).to.equal(FAKE_TABLE_NAME);
            expect(params.Key).to.deep.equal({ Key: key })

            cb(null, { Item: item });
        });

        // Execute
        const actual = await repository.getByKey(key);

        // Test
        expect(actual).to.deep.equal(item);*/
    });

    it('rejects with a NOT FOUND error when no item exists', sandbox(async () => {
        // TODO
    }));

    it('rejects errors when returned by DDB', sandbox(async () => {
        // TODO
    }));
});

describe('Repository List', () => {
    it('returns a paginated list of items', () => {
        // TODO
    });

    it('rejects erros when returned by DDB', () => {
        // TODO
    });
});

describe('Repository Save', () => {
    it('updates an item in DDB if it exists', () => {
        // TODO
    });

    it('throws a NOT FOUND error if an item does not already exist', () => {
        // TODO
    });

    it('throws any other error directly, if unrelated to the item not existing in DDB', () => {
        // TODO
    });
});
const { UserRepository } = require('otf-data-access');
const { RandomUserGenerator } = require('otf-accessors');

const createRandomUser = async () => {
    const user = await RandomUserGenerator.generate();
    await UserRepository.create(user);
    return user;
};

const getUserById = async (id) => {
    return await UserRepository.getById(id);
};

const listUsers = async (pagination) => {
    return await UserRepository.list(pagination);
};

const updateUser = async (id, user) => {
    user.id = id;
    await UserRepository.update(user);
};

module.exports = {
    /**
     * Creates a new random user
     * @returns the created user
     */
    createRandomUser: createRandomUser,
    /**
     * Retrieve a user by its id
     * @param {String} id the unique identifier
     * @returns the user, if it exists
     * @throws NotFoundError if the user does not exist
     */
    getById: getUserById,
    /**
     * Updates the attributes of a user
     * @param {String} id the id of the user to update
     * @param {Object} user the user object containing the new attribute values
     * @throws NotFoundError if the user does not exist
     */
    update: updateUser,
    /**
     * Retrieves a list of users
     * @param {Object} pagination the pagination parameters of the request
     * @returns an array of user objects
     */
    list: listUsers
};
const { expect } = require('chai');
const sinon = require('sinon');
const sandbox = require('sinon-test')(sinon);
const { UserRepository } = require('otf-data-access');
const { RandomUserGenerator } = require('otf-accessors');
const userComponent = require('../user-component');

describe('User component', () => {
    it('Creates a random user', sandbox(async function () {
        // Setup
        const randomUserApiMock = this.mock(RandomUserGenerator);
        const userRepoMock = this.mock(UserRepository);

        const user = { name: "Aegon" };
        randomUserApiMock.expects('generate').once().resolves(user);
        userRepoMock.expects('create').withArgs(user);

        // Execute
        const actual = await userComponent.createRandomUser();

        // Test
        expect(user).to.deep.equal(actual);
    }));

    it('Gets a user by its id', sandbox(async function () {
        // Setup
        const user = { name: "Rob" };
        const id = 4;
        const userRepoMock = this.mock(UserRepository);
        userRepoMock.expects('getById').withArgs(id).resolves(user);

        // Execute
        const actual = await userComponent.getById(id);

        // Test
        expect(user).to.deep.equal(actual);
    }));

    it('Retrieves a list of users', sandbox(async function () {
        // Setup
        const users = [{ name: "Theon" }, { name: "Reek" }];
        const pagination = { maxResults: 40 };
        const userRepoMock = this.mock(UserRepository);
        userRepoMock.expects('list').withArgs(pagination).resolves(users);

        // Execute
        const actual = await userComponent.list(pagination);

        // Test
        expect(users).to.equal(actual);
    }));

    it('Updates a user', sandbox(async function () {
        // Setup
        const id = 4;
        const user = { name: "Jorah" };
        const userRepoMock = this.mock(UserRepository);
        userRepoMock.expects('update').withArgs({
            name: user.name,
            id: id
        });

        // Execute + Test
        await userComponent.update(id, user);
    }));
});
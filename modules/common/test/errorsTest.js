const { expect, assert } = require('chai');
const errors = require('../errors');

describe('Error codes', () => {
    it('contains a NotFound (404) error', () => {
        const error = errors.NOT_FOUND_ERROR;

        assert.isTrue(error instanceof Error);
        expect(error.message).to.equal("Not Found");
        expect(error.httpCode).to.equal(404);
    });
});

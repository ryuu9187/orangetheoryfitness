const createError = (message, httpCode) => {
    const error = new Error(message);
    error.httpCode = httpCode;
    return error;
};

module.exports = {
    NOT_FOUND_ERROR: createError("Not Found", 404)
};
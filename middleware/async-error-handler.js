const { formatErrorBody } = require('../utils/response-utils');

const handleError = (res) => {
    return (ex) => {
        // Log error
        console.error(ex.message);

        // Return proper HTTP code + user-friendly message
        const errorMessage = ex.httpCode ? ex.message : "Internal Server Error";
        const httpCode = ex.httpCode || 500;

        res.status(httpCode).json(formatErrorBody(errorMessage));
    };
};

/**
 * Error handling/translation wrapper for executing asynchronous API implementations
 */
module.exports = (route) => {
    return (req, res, next) => {
        return Promise
            .resolve(route(req, res, next))
            .catch(handleError(res));
    };
};
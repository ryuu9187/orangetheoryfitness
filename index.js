const serverless = require('serverless-http');
const bodyParser = require('body-parser');
const express = require('express');
const app = express();

// Load Global Middleware
app.use(bodyParser.json({ strict: false }));

// Load Routes
require("./routes/user-route")(app);

app.get('/', function (req, res) {
  res.send(`This is the root URL for Jason Braswell's Orange Theory Fitness code assignment.`);
});

module.exports.handler = serverless(app);